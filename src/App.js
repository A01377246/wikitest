import React, { useState, useEffect } from "react";
import "./App.css";
import Expenses from "./components/Expenses";
import NewExpense from "./components/NewExpense/NewExpense";
import { db } from "./firebaseConfig";
import { collection, getDocs } from "firebase/firestore";
import "./resources/images/fondoLab.png";

//Trigger something
/*<div className="App">
      <header className="App-header">
        <img src={loadingBottle} className="App-logo" alt="logo" />
        <p>IGEM TEC CEM</p>
        <ExpenseItem />
      </header>
    </div>*/

const activityCollectionReference = collection(db, "Activities");

// Your web app's Firebase configuration

function App() {
  const [expenses, setExpenses] = useState([]);

  useEffect(() => {
    const getActivities = async () => {
      const data = await getDocs(activityCollectionReference);
      setExpenses(data.docs.map((doc) => ({ ...doc.data(), id: doc.id })));
      console.log(data);
    };

    getActivities();
  }, []);

  const addExpenseHandler = (expense) => {
    setExpenses((prevExpenses) => {
      return [expense, ...prevExpenses];
    });
  };

  return (
    <div className="labBg">
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses items={expenses} />
    </div>
  );
}

export default App;
